package com.profile.linkedin;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProfileLinkedinApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProfileLinkedinApplication.class, args);
	}

}
